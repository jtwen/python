~~~markdown

[TOC]

《编程小白的第1本Python入门书》学习笔记
## 函数
python3.5一共68个内建函数，简单来讲是安装完就可以使用，是“自带的”。
如print(),input(),len(),int()等

### 开始创建函数
几个常见的词：
def(define，定义)：定义一个函数
arg(argument，参数)：有时还有parameter，二者都是参数的意思，稍有不同
return：返回结果

**def**和**return**是关键字(keyword)
```python
# 定义一个函数
def fahrenheit_converter(C):
    fahrenheit = C * 9 / 5 + 32
    return str(fahrenheit) + '˚F'


C2F = fahrenheit_converter(35)
print(C2F)  # 95.0˚F


# 对函数作出修改
def fahrenheit_converter(C):
    fahrenheit = C * 9 / 5 + 32
    print(str(fahrenheit) + '˚F')


print(fahrenheit_converter(35))
```
print是一个函数并非关键字

## 练习题
1. 设计一个重量转换器，输入以“g”为单位的数字后返回换算成“kg”的结果
2. 设计一个求直角三角形斜边长的函数（两条直角边为参数，求最长边）

## 传递参数与参数类型
对于在一开始就设定了必要参数的函数来说，打出函数名并向括号中传递参数实现对函数的调用（call），只要把参数放进函数的括号中即可
```python
def fahrenheit_converter(C):
    fahrenheit = C * 9 / 5 + 32
    return str(fahrenheit) + '˚F'

fahrenheit_converter(35)
fahrenheit_converter(15)
fahrenheit_converter(0)
fahrenheit_converter(-3)
```

实际上传递参数有两种方式：位置参数和关键词参数
```python
def trapezoid_area(base_up, base_down, height):
    return 1 / 2 * (base_up + base_down) * height

print(trapezoid_area(1, 2, 3))  # 4.5（位置参数）
print(trapezoid_area(base_up=1, base_down=2, height=3))  # 4.5（关键词参数）

# trapezoid_area(height=3, base_down=2, base_up=1)  # 关键词参数反序输入并不影响函数正常运作
# trapezoid_area(height=3, base_down=2, 1)  # 错误语法，最后一个位置应该是参数height的位置，但与前面height已经按名称传入的3冲突
# trapezoid_area(base_up=1, base_down=2, 3)  # 参数正序传入，前两个关键词的方式，最后一个以位置参数传入，可正常运行
# trapezoid_area(1, 2, height=3)  # 参数正序传入，前两个以位置方式传入，最后一个以关键字参数传入，可正常运行

# 给一组变量赋值然后调用

```
# 5 循环与判断
## 5.1逻辑控制与循环
### 5.1.1 布尔类型：True & False(首字母一定要大写)

使用命令行/终端只为更快展示结果，在IDE返回布尔值仍需使用print函数来实现
```python
1>2  # False
1<2<3  # True
42 != '42'  # True
'Name' == 'name'  # False
'M' in 'Magic'  # True
number = 12  # 没有输出
number is 12  # python3不能这样写
```
### 5.1.2 比较运算(Comparison)
比较式成立返回True，不成立返回False
除此之外，还有多条件的比较。先给变量赋值再比较

**注**： abs()表示取绝对值函数
* 比较运算的一些小问题：

> 不同类型的对象不能使用“<,>,<=,>=”进行比较，但可用‘==’和‘！=’
> 浮点数和整数虽是不同类型，但不影响到比较运算
> True = 1, False = 0
> 1 <> 3 其实等价于 1 ！= 3

### 5.1.3 成员运算符与身份运算符
in & is
 - in后面是一个集合形态的对象，字符串满足此特性，因此可用in进行测试
 - is 和 is not是表示身份鉴别的布尔运算符，in 和 not in表示归属关系的布尔运算符号
 - python中任何一个对象都要满足身份、类型、值这三点。is用来进行身份对比
 
 ```python
# the_Eddie = 'Eddie'
# name = 'Eddie'
# the_Eddie == name
# the_Eddie is name
```
两个变量一致时经过is对比返回True，在python中任何对象都可判断其布尔值，除了0、None和所有空序列与集合（列表、字典、集合）布尔值为False之外其他都为
True。可使用函数bool()进行判别

### 5.1.4 布尔运算符 and、or

## 5.2 条件控制 if ...else
```python
# 给函数增加一个重置密码的功能
password_list = ['*#*#', '12345']  # 创建一个列表，存储用户的密码 、初始密码和其他数据（对实际用户数据库的简单模拟）
def account_login():  # 定义函数
    password = input('Password:')  # 获得字符串并存在变量中
    password_correct = password == password_list[-1]  # 当用户输入的密码等于密码列表中最后一个元素时（即用户最新设定的密码）登录成功
    password_reset = password == password_list[0]
    if password_correct:
        print('Login success!')
    elif password_reset:
        new_password = input("Enter a new password:")  # 当用户输入的密码等于密码列表中第一个元素时（重置密码的“口令”）触发密码变更，并
        # 将变更后的密码存储至列表的最后一个，成为最新的用户密码
        password_list.append(new_password)
        print('Your password has changed successfully!')
        account_login()
    else:
        print('Wrong password or invalid input!')  # 一切不等于预设密码的输入结果，会执行打印错误提示，并且再次调用函数，让用户再次输入
        account_login()


account_login()
```
## 5.3 循环（Loop）
### 5.3.1 for
```python
for every_letter in 'Hello world':
    print(every_letter)

songslist = ['Holy Diver', 'Thunderstruck', 'Rebel Rebel']
for song in songslist:
    if song == 'Holy Diver':
        print(song, ' - Dio')
    elif song == 'Thunderstruck':
        print(song, ' - AC/DC')
    elif song == 'Rebel Rebel':
        print(song, ' - David Bowie')  # songslist列表中的每一个元素依次取出，分别与三个条件比较，若成立输出相应的内容

```
### 5.3.2 嵌套循环(Nested Loop)
```python
for i in range(1, 10):
    for j in range(1, 10):
        print('{} X {} = {}'.format(i, j, i * j))
```

### 5.3.3 while循环

# 6 数据结构
## 6.1 数据结构(Data Structure)
存储大量数据的容器在python称之为内置数据结构(Built-in Data Structure)

python 有4种数据结构：列表、字典、元组、集合
```python
# list = [val1, val2, val3, val4]
# dict = {key1:val1, key2:val2}
# tuple = (val1, val2, val3, val4)
# set = {val1, val2, val3, val4}
```

## 6.2 列表(list)
显著特征：

1. 列表中的每一个元素是可变的；
2. 列表中的元素是有序的；
3. 列表可以容纳python中的任何对象。

## 6.3 字典(Dictionary)
显著特征：

1. 字典中数据必须是以键值对形式出现；
2. 逻辑上讲，键不可以重复，值可以重复；
3. 键(key)是不可变的，即无法修改的，值(value)是可变的，可修改的，可以是任何对象。

## 6.4 元组(Tuple)
可理解成稳固版的列表，因元组不可修改，因此在列表中存在的方法均不可用在元组上，但元组可以被查看索引，方式同列表
```python
letters = ('a', 'b', 'c', 'd', 'e', 'f', 'g')
letters[0]
```
## 6.5 集合(Set)
每个集合中的元素是无序的、不重复的任意对象。可通过集合判断数据的从属关系，还可通过集合把数据结构中重复的元素减掉。

集合不能被切片也不能被索引，除了做集合运算外，集合元素可被添加和删除
```python
a_set = {1, 2, 3, 4}
a_set.add(5)
a_set.discard(5)
```

## 6.6 数据结构的一些技巧
### 6.6.1 多重循环

```python
num_list = [6, 2, 7, 4, 1, 3, 5]
print(sorted(num_list))  # sort函数按照长短、大小、英文字母的顺序给每个列表中的元素进行排序；它并不会改变列表本身，可理解为先将列表复制然后再进行顺序的整理
                         # [1, 2, 3, 4, 5, 6, 7]
print(num_list.reverse)  # 函数reverse()不返回列表而返回一个迭代器，可使用list将其转换为列表
print(list(reversed(num_list)))  # [5, 3, 1, 4, 7, 2, 6]

str = [12, 13, 56, 43, 58]

# 如果同时需要两个列表，用zip()函数
for a, b in zip(num_list, str):
    print(b, 'is', a)

```

### 6.6.2 推导式(List comprehension)--列表的解析式
```python
# 普通写法
a = []
for i in range(1, 11):
    print(a.append(i))

# 列表解析式
b = [i for i in range(1, 11)]
print(b)

```

列表解析式方便，且执行效率高，对比两者所耗费的时间
```python
import time

a = []
t0 = time.process_time()
for i in range(1, 200000):
    a.append(i)
print(time.process_time() - t0, "seconds process time")

t0 = time.process_time()
b = [i for i in range(1, 200000)]
print(time.process_time() - t0, "seconds process time")

```

列表推导式

```python
# 列表推导式
a = [i ** 2 for i in range(1, 10)]
c = [j + 1 for j in range(1, 10)]
k = [n for n in range(1, 10) if n % 2 == 0]
z = [letter.lower() for letter in 'ABCDEFGHIGKLMN']

d = {i:i+1 for i in range(4)}
g = {i:j for i, j in zip(range(1, 6), 'abcde')}
g = {i:j.upper() for i, j in zip(range(1,6), 'abcde')}

print(a, c, k, z, d, g)
```

### 6.6.3 循环列表时获取元素的索引

列表是有序的，可使用python中独有的函数enumerate来进行
```python
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
for num, letter in enumerate(letters):
    print(letter, 'is', num + 1)

```

# 7 类与可口可乐
## 7.1 定义一个类
类是有一些系列有共同特征和行为事物的抽象概念的总和
```python

class CocaCola:
    formula = ['caffeine',  'sugar', 'water', 'soda']
```
使用class定义一个类，缩进的地方formula变量装载着列表，在类里赋值的变量就是类的变量——类的属性(Class Attribute)。字符串、列表、字典、整数等都可以是变量，都可成为类的属性。

## 7.2 类的实例化
```python

# coke_for_me = CocaCola()
# coke_for_you = CocaCola()

# print(CocaCola.formula)
# print(coke_for_me.formula)
# print(coke_for_you.formula)

# ['caffeine', 'sugar', 'water', 'soda']
# ['caffeine', 'sugar', 'water', 'soda']
# ['caffeine', 'sugar', 'water', 'soda']
```
左边创建变量，右边写上类的名称 -- 实例化，被实例化之后的对象 -- 实例(instance)

## 7.3 类属性引用
在类名后输入. ，可引用之前在定义类时的属性；**类的属性会被所有类的实例共享**，所以在类的实例后再点上. ，索引用的属性值是完全一样的。

类的属性与正常的变量并无区别。

## 7.4 实例属性

```python

class CocaCola:
    formula = ['caffeine',  'sugar', 'water', 'soda']


coke_for_China = CocaCola()
coke_for_China.local_logo = '可口可乐'  # 创建实例属性

print(coke_for_China.local_logo)  #打印实例属性引用结果
```

- 创建类之后，通过 `object.attr` 的形式进行一个赋值，得到一个新的实例的变量 -- 实例变量 -- 实例属性(Instance Attribute)

**实例方法**
方法(Method) -- 供实例使用 -- 也称**实例方法(Instance Method)**，使用类的方法来表示某种“功能”

```python
class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']

    def drink(self):
        print('Energy!')

```

- 注：英文中“功能”和“函数”均为-- Function
- self? 修改一下代码
```python
class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']

    def drink(coke):  #HERE!
        print('Energy!')

coke = CocaCola()
coke.drink()  # Energy!

```
- 这个参数就是被创建的实例本身 <-- 第四章中函数的使用办法 -- 将一个个对象作为参数放入函数括号内
- 一旦一个类被实例化，我们可使用类似函数的方式
```python
# coke = CocaCola
# coke.drink() == CocaCola.drink(coke)  # 左右两边写法完全一致
```
> 被实例化的对象会被编译器默默传入后面方法的括号中，作为第一个参数。实际中更多使用前面那种形式。self这个参数名称是可以更改的(编译器不会因此报错)，但
> 按照python规矩统一使用 `self`

## 7.5 更多参数
类的方法也能有属于自己的参数，做些改动：
```python
class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']

    def drink(self, how_much):

        if how_much == 'a sip':
            print('Cool!!!)
        elif how_much == 'whole bottle':
            print('Headache!')

ice_coke = CocaCola()
ice_coke.drink('a sip')  # Cool!!!
```
**"魔术方法"** <-- __init__()是其中一个

如果在类里定义，在创建实例时就会自动的处理很多事情，如新增实例属性。

```python

class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']
    def __init__(self):
        self.local_logo = '可口可乐'

    def drink(self):  #HERE!
        print('Energy!')

coke = CocaCola()
print(coke.local_logo)  # 可口可乐
```

- __init()__方法给类的使用提供了极大的灵活性，尝试以下代码

```python
class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']
    def __init__(self):
        for element in self.formula:
            print('Coke has {}!'.format(element))

    def drink(self):
        print('Energy!')

coke = CocaCola
```

除了必写的self参数外，__init__()可以有自己的参数，在实例化时往括号里放参数，相应的所有参数都会传递到__init__()方法中，和函数的参数的用法完全相同。

```python

class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']
    def __init__(self, logo_name):
        self.local_logo = logo_name

    def drink(self):
        print('Energy!')

coke = CocaCola('可口可乐')
print(coke.local_logo)  # 可口可乐

```

`self.local_logo = logo_name`简单解释：左边是变量作为类的属性，右边是传入的参数 -- 作为变量，即这个变量的赋值所存储的结果将取决于初始化时所传
进来的参数 `logo_name` 

## 7.6 类的继承
```python
class CocaCola:
    calories = 140
    sodium = 45
    total_carb = 39
    caffeine = 34
    ingredients = [
        'High Fructose Corn Syrup',
        'Carbonated Water',
        'Phosphoric Acid',
        'Natural Flavors',
        'Caramel Color',
        'Caffeine'
    ]

    def __init__(self, logo_name):
        self.local_logo = logo_name

    def drink(self):
        print('You got {} cal energy!'.format(self.calories))

# 类的继承(Inheritance)，拿无卡可乐(caffeine-free)作为例子

class CaffeineFree(CocaCola):  # 括号中放入CocaCola表示这个类是继承于CocaCola这个父类的。CaffeineFree成为了CocaCola的子类。
    caffeine = 0
    ingredients = [
        'High Fructose Corn Syrup',
        'Carbonated Water',
        'Phosphoric Acid',
        'Natural Flavors',
        'Caramel Color',
    ]

coke_a = CaffeineFree('Cocacola-FREE')

coke_a.drink()
```

类中的变量和方法可以完全被子类继承，如需有特殊改动也可进行**覆盖(Override)**

## 7.7 令人困惑的类属性与实例属性

> Q1 类属性如果被重新赋值，是否会影响到类属性的引用？
```python
class TestA:
    attr = 1
obj_a = TestA()

TestA.attr = 42
print(obj_a.attr)  # 42
```
> Q2 实例属性如果被重新赋值，是否会影响到类属性的引用？
```python
class TestA:
    attr = 1
obj_a = TestA()
obj_b = TestA()

obj_a.attr = 42
print(obj_b.attr)  # 1

```

> Q3 类属性实例属性具有相同的名称，那么`.`之后引用的将会是什么？
```python
class TestA:
    attr = 1
    def __init__(self):
        self.attr = 42

obj_a = TestA()
obj_b = TestA()

print(obj_b.attr)  # 42
```

- 更为直接的解释隐藏在类的特殊属性`__dict__` 中。`__dict__` 是一个类的特殊属性，它是一个字典，用于存储类或实例的属性。即使不去定义，它也会存储在
每一个类中，是默认隐藏的。在Q3中添加下面两行
`print(TestA.__dict__)`
`print(obj_a.__dict__)`

\>>> {'__module__': '__main__', 'attr': 1, '__init__': <function TestA.__init__ at 0x000002D53D05D310>, '__dict__'
: <attribute '__dict__' of 'TestA' objects>, '__weakref__': <attribute '__weakref__' of 'TestA' objects>, '__doc__':
 None}

\>>> {'attr': 42}

其中，类TestA和类的实例obj_a各自拥有各自的attr属性，完全独立

python中属性的引用机制是自外而内的，创建了一个实例后，编译器会先搜索该实例是否有该属性 --> 如果没有，搜索这个实例所属的类是否有该属性 --> 如果
还没有，报错

## 7.8 类的扩展理解
```python
obj1 = 1
obj2 = 'String!'
obj3 = []
obj4 = {}

print(type(obj1), type(obj2), type(obj3), type(4))  # <class 'int'> <class 'str'> <class 'list'> <class 'int'>

```
python中任何种类的对象都是类的实例，上面的这些类型被称为内建类型，并不需要实例化。

制作一个填充用户假数据的小工具
父类：FakeUser
功能：
1. 随机姓名
  a. 单字名
  b. 双字名
  c. 其他，有个名字就好
2. 随机性别

子类：SnsUser
功能：
1. 随机数量的跟随者
  a. few
  b. a lot
  
# 8 开始使用第三方库

## 8.1 强大的第三方库
搭建网站 -- Django、轻量的Flask等Web框架；

写小游戏 -- PyGame;

爬虫 -- Scrapy框架；

数据统计分析 -- Pandas数据框架...

- 根据自己需求按分类找到相应的库
 [https://awesome-python.com/]()  比如想找爬虫方面的，就选择Web Crawling这个分类，即可看到相应的第三方库的网站与简介
 
- 直接使用搜索引擎
## 8.2 安装第三方库
### 在Pycharm中安装

1. File --> Default Settings
2. 搜索project interpreter，选择当前的python环境，点击左下角加号 + 
3. 输入想要安装的库的名称，勾选并点击Install Package
4. 安装成功后会有...successfully的提示，在Project Interpreter页面可查看安装了哪些库，点击减号 - 可删除不需要的。

### 在终端/命令行中安装

```
# 在终端/命令行中输入
pip -version  # 查看pip版本
pip install PackageName  # 安装PackageName
pip inatall --upgrade pip  # 升级pip
pip uninstall flask  # 卸载库
pip list  # 查看已安装库
```

## 8.4 使用第三方库

在pycharm中输入库的名字，会提示自动补全

`import pandas`  导入pandas库，灰色的代表还没有在程序中使用。要检查是否安装可使用pip的方式确认

# 9 编程小白的学习资源

## 资料库参考

1. safari online book [https://www.safaribooksonline.com]()
2. 图灵社区 [http://www.ituring.com.cn]()
3. Stackoverflow [http://stackoverflow.com]()
4. Python官方文档 [http://python.usyiyi.cn/python_343/tutorial/index.html]()

...