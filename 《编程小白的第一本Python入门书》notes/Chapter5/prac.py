import random

"""
# 1. 创建10个文本并以数字命名
def text_creation():
    path = 'C://Users/janty/desktop/w/w'  # 在桌面的w文件夹中写入
    for name in range(1, 11):
        with open(path + str(name) + '.txt', 'w') as text:
            text.write(str(name))
            text.close()
            print('Done')


text_creation()



# 2. 设计一个复利函数invest()，包含三个参数：amount(资金)，rate(利率)，time(投资时间)。输入每个参数后调用函数，应该返回每一年的总金额
# 假设利率为5%
def invest(amount, rate, time):
    print("principal amount:{}".format(amount))
    for t in range(1, time + 1):
        amount = amount * (1 + rate)
        print("year {}: ${}".format(t, amount))


invest(100, .05, 8)
invest(2000, .025, 5)


# 3. 打印1 ~ 100内的偶数
def even_print():
    for i in range(1, 101):
        if i % 2 == 0:
            print(i)


even_print()

# 综合练习
# 设计一个小游戏猜大小：首先创建一列表放入数字再使用sum()函数对列表中所有整数求和，然后打印
a_list = [1, 2, 3]
print(sum(a_list))

point1 = random.randrange(1, 7)  # random内置库，使用它生成随机数
point2 = random.randrange(1, 7)
point3 = random.randrange(1, 7)

print(point1, point2, point3)




# 细化游戏规则：开始时玩家选择Big or Small（押大小）选择完成后开始摇三个骰子计算总值，11 <= 总值 <= 18 为“大”，3 <= 总值 <= 10 为“小”，然后
# 告诉玩家猜对或错的结果
def roll_dice(numbers=3, points=None):
    print('<<<<< ROLL THE RICE! >>>>>')
    if points is None:
        points = []
    while numbers > 0:
        point = random.randrange(1, 7)
        points.append(point)
        numbers = numbers - 1
    return points


def roll_result(total):
    isBig = 11 <= total <= 18
    isSmall = 3 <= total <= 10
    if isBig:
        return 'Big'
    elif isSmall:
        return 'Small'


def start_game():
    print('<<<<< GAME STARTS! >>>>>')
    choices = ['Big', 'Small']
    your_choice = input('Big or Small :')
    if your_choice in choices:
        points = roll_dice()
        total = sum(points)
        youWin = your_choice == roll_result(total)
        if youWin:
            print('The points are',points,'You win!')
        else:
            print('The points are',points,'You lose!')
    else:
        print('Invalid Words')
        start_game()
start_game()




# 练习
# 1. 在最后一个项目增加这样的功能：下注金额和赔率。初始金额为1000元，金额为0时游戏结束，默认赔率为1倍，押对了可得相应金额，押错了会输掉相应金额
def roll_dice(numbers=3, points=None):
    print('<<<<< ROLL THE RICE! >>>>>')
    if points is None:
        points = []
    while numbers > 0:
        point = random.randrange(1, 7)
        points.append(point)
        numbers = numbers - 1
    return points


def roll_result(total):
    isBig = 11 <= total <= 17
    isSmall = 4 <= total <= 10
    if isBig:
        return 'Big'
    elif isSmall:
        return 'Small'


def start_game():
    your_money = 1000
    while your_money > 0:
        print('<<<<< GAME STARTS! >>>>>')
        choices = ['Big', 'Small']
        your_choice = input('Big or Small :')

        if your_choice in choices:
            your_bet = int(input('How much you wanna bet? - '))
            points = roll_dice()
            total = sum(points)
            youWin = your_choice == roll_result(total)
            if youWin:
                print('The points are', points, 'You win!')
                print('You gained {}, you have {} now'.format(your_bet, your_money + your_bet))
                your_money = your_money + your_bet
            else:
                print('The points is', points, 'You lose!')
                print('You lost {}, you have {} now'.format(your_bet, your_money - your_bet))
                your_money = your_money - your_bet
        else:
            print('Invalid Words')
    else:
        print('GAME OVER!')


start_game()

"""


# 练习2：验证手机号：长度不少于11位；是移动、联通、电信号段中的一个电话号码；输入除号码外其他字符的可能性忽略；
# 号段已知：
def number_test():
    while True:
        number = input('Enter Your number :')
        CN_mobile = [134, 135, 136, 137, 138, 139, 150, 151, 152, 157, 158, 182, 183, 184, 187, 188, 147, 178, 1705]
        CN_union = [130, 131, 132, 155, 156, 185, 186, 145, 176, 1709]
        CN_telecom = [133, 153, 180, 181, 189, 177, 1700]
        first_three = int(number[0:3])
        first_four = int(number[0:4])

        if len(number) == 11:
            if first_three in CN_mobile or first_four in CN_mobile:
                print('Operator : China Mobile')
                print('We\'re sending verification code via text to your phone:',number)
                break
            elif first_three in CN_telecom:
                print('Operator : China Telecom')
                print('We\'re sending verification code via text to your phone:', number)
                break
            elif first_three in CN_union:
                print('Operator : China Union')
                print('We\'re sending verification code via text to your phone:', number)
                break
            else:
                print('No such a operator')
        else:
            print('Invalid length, your number should be in 11 digits')


number_test()

# 在程序中，常有一些无限循环的情况，如一个程序没有异常发生时，让循环一直执行，此时需要用while(true)...break 这种用法
# 如果使用了 if first_three or first_four in CN_mobile: 会一直得到CN_mobile的结果，这是因为在布尔运算中130 or 1301为True

