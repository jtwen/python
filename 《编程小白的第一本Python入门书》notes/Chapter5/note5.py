"""
def account_login():
    password = input('Password:')
    if password == '12345':
        print('Login success!')
    else:
        print('Wrong password or invalid input!')
        account_login()


account_login()


# 若if后的布尔表达式过长或者难于理解，可给变量赋值，存储布尔表达式返回的布尔值True或False
def account_login():
    password = input('Password:')
    password_correct = password == '12345'
    if password_correct:
        print('Login success!')
    else:
        print('Wrong password or invalid input!')
        account_login()


account_login()

# 给函数增加一个重置密码的功能
password_list = ['*#*#', '12345']  # 创建一个列表，存储用户的密码 、初始密码和其他数据（对实际用户数据库的简单模拟）


def account_login():  # 定义函数
    password = input('Password:')  # 获得字符串并存在变量中
    password_correct = password == password_list[-1]  # 当用户输入的密码等于密码列表中最后一个元素时（即用户最新设定的密码）登录成功
    password_reset = password == password_list[0]
    if password_correct:
        print('Login success!')
    elif password_reset:
        new_password = input("Enter a new password:")  # 当用户输入的密码等于密码列表中第一个元素时（重置密码的“口令”）触发密码变更，并
        # 将变更后的密码存储至列表的最后一个，成为最新的用户密码
        password_list.append(new_password)
        print('Your password has changed successfully!')
        account_login()
    else:
        print('Wrong password or invalid input!')  # 一切不等于预设密码的输入结果，会执行打印错误提示，并且再次调用函数，让用户再次输入
        account_login()


account_login()


for num in range(1, 11):  # 不包含11，因此实际范围是1 ~ 10
    print(str(num) + ' + 1 =', num + 1)

songslist = ['Holy Diver', 'Thunderstruck', 'Rebel Rebel']
for song in songslist:
    if song == 'Holy Diver':
        print(song, ' - Dio')
    elif song == 'Thunderstruck':
        print(song, ' - AC/DC')
    elif song == 'Rebel Rebel':
        print(song, ' - David Bowie')  # songslist列表中的每一个元素依次取出，分别与三个条件比较，若成立输出相应的内容


for i in range(1, 10):
    for j in range(1, 10):
        print('{} X {} = {}'.format(i, j, i * j))


while 1 < 3:
    print('1 is smaller than 3')  # 死循环（Infinite Loop），一定要及时停止运行代码【在终端或命令行中按Ctrl + C停止运行】

# 在循环中制造某种可以使循环停止的条件
count = 0
while True:
    print('Repeat this line !')
    count = count + 1
    if count == 5:
        break

"""
# 改变循环成立的条件，在前面登录函数基础上增加一个新功能：输入密码错误禁止再次输入

password_list = ['*#*#', '12345']  # 创建一个列表，存储用户的密码 、初始密码和其他数据（对实际用户数据库的简单模拟）


def account_login():  # 定义函数
    tries = 3
    while tries > 0:
        password = input('Password:')  # 获得字符串并存在变量中
        password_correct = password == password_list[-1]  # 当用户输入的密码等于密码列表中最后一个元素时（即用户最新设定的密码）登录成功
        password_reset = password == password_list[0]
        if password_correct:
            print('Login success!')
        elif password_reset:
            new_password = input("Enter a new password:")  # 当用户输入的密码等于密码列表中第一个元素时（重置密码的“口令”）触发密码变更，并
        # 将变更后的密码存储至列表的最后一个，成为最新的用户密码
            password_list.append(new_password)
            print('Your password has changed successfully!')
            account_login()
        else:
            print('Wrong password or invalid input!')  # 一切不等于预设密码的输入结果，会执行打印错误提示，并且再次调用函数，让用户再次输入
            tries = tries - 1
            print(tries, 'times left')
    else:
        print('Your account has been suspended')


account_login()
