"""
# 列表可以装入python中所有的对象
all_in_list = [
    1,                  # 整数
    1.0,                # 浮点数
    'a word',           # 字符串
    print(1),           # 函数
    True,               # 布尔值
    [1, 2],             # 列表中套列表
    (1, 2),             # 元组
    {'key': 'value'}    # 字典
]

# 列表的增删改查
fruit = ['pineapple', 'pear']
# fruit.insert(1, 'grape')  # 插入元素的实际位置是在指定位置元素之前的位置。如果指定插入位置不存在，即超出了指定列表长度

fruit[0:0] = ['Orange']  # 这种方法也可插入
print(fruit)

fruit.remove('Orange')  # 删除列表中元素
print(fruit)

fruit[0] = 'Grapefruit'  # 替换修改其中的元素
print(fruit)

del fruit[0:2]  # 删除的另一种方式，使用del关键字声明
print(fruit)

# 列表只接受用位置进行索引，但如果数据量大，不适宜用列表


NASAQ_code = {
    'BIDU': 'Baidu',
    'SINA': 'Sina',
    'YOKU': 'Youku'
}
NASAQ_code['TEST'] = 'test'  # 添加元素
print(NASAQ_code)

NASAQ_code.update({'FB':'facebook','TSLA':'Tesla'})  # 添加多个元素的方法update()
print(NASAQ_code)

del NASAQ_code['FB']  # 删除字典中的元素
print(NASAQ_code)

# 虽然字典使用花括号，在索引内容时仍旧使用和列表一样的方括号来进行索引，括号放字典中的键，即通过键索引值
# 字典不能切片

letters = ('a', 'b', 'c', 'd', 'e', 'f', 'g')
print(letters[0])


a_set = {1, 2, 3, 4}
a_set.add(5)

print(a_set)
a_set.discard(5)
print(a_set)



num_list = [6, 2, 7, 4, 1, 3, 5]
print(sorted(num_list))  # sort函数按照长短、大小、英文字母的顺序给每个列表中的元素进行排序；它并不会改变列表本身，可理解为先将列表复制然后再进行顺序的整理
                         # [1, 2, 3, 4, 5, 6, 7]
print(num_list.reverse)  # 函数reverse()不返回列表而返回一个迭代器，可使用list将其转换为列表
print(list(reversed(num_list)))  # [5, 3, 1, 4, 7, 2, 6]


str = [12, 13, 56, 43, 58]
num_list = [6, 2, 7, 4, 1, 3, 5]
# 如果同时需要两个列表，用zip()函数
for a, b in zip(num_list, str):
    print(b, 'is', a)

a = []
for i in range(1, 11):
    print(a.append(i))

# 列表解析式
b = [i for i in range(1, 11)]
print(b)


import time

a = []
t0 = time.process_time()
for i in range(1, 200000):
    a.append(i)
print(time.process_time() - t0, "seconds process time")

t0 = time.process_time()
b = [i for i in range(1, 200000)]
print(time.process_time() - t0, "seconds process time")



# 列表推导式
a = [i ** 2 for i in range(1, 10)]
c = [j + 1 for j in range(1, 10)]
k = [n for n in range(1, 10) if n % 2 == 0]
z = [letter.lower() for letter in 'ABCDEFGHIGKLMN']

d = {i:i+1 for i in range(4)}
g = {i:j for i, j in zip(range(1, 6), 'abcde')}
g = {i:j.upper() for i, j in zip(range(1,6), 'abcde')}

print(a, c, k, z, d, g)


letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
for num, letter in enumerate(letters):
    print(letter, 'is', num + 1)

"""



