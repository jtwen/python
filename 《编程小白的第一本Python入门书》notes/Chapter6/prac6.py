"""
# 综合项目：词频统计，需要瓦尔登湖的文本，下载后用pycharm打开文本重新保存一次避免编码问题
# 准备知识
lyric = 'The night begin to shine, the night begin to shine'
words = lyric.split()  # 使用split方法将字符串中的每个单词分开得到独立的单词
print(words)  # ['The', 'night', 'begin', 'to', 'shine,', 'the', 'night', 'begin', 'to', 'shine']


# 词频统计，使用count方法统计重复出现的单词
path = 'C://Users/janty/Downloads/Walden.txt'
with open(path, 'rb') as text:
    words = text.read().split()
    print(words)
    for word in words:
        print('{}-{} times'.format(word, words.count(word)))

    """
# 有一些带标点符号的被单独统计了次数；有些单词不止一次展示了出现的次数；开头大写的单词被统计了。调整之后，如下
import string  # 引入新的模块string
path = 'C://Users/janty/Downloads/Walden.txt'

with open(path, 'rb') as text:
    words = [raw_word.strip(string.punctuation).lower() for raw_word in text.read().decode().split()]
    # 在文字首位去掉了连在一起的标点符号，并把首字母大写的单词转化为小写；
    # 通常从网络或磁盘上读取的数据是bytes，bytes变为字符str: decode()转成字符str。
    # 向磁盘写文件时，传入的是字节流，将str转成encode()字节流
    words_index = set(words)  # set将其转换为集合，去重
    counts_dict = {index: words.count(index) for index in words_index}  # 创建一个以单词为键(key)，出现频率为值(value)的字典

for word in sorted(counts_dict, key=lambda x: counts_dict[x], reverse=True):  # lambda表达式，暂理解为以字典中的值为排序的参数
    print('{}-{} times'.format(word, counts_dict[word]))


