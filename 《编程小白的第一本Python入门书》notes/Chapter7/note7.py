"""
class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']


coke_for_me = CocaCola()
coke_for_you = CocaCola()

print(CocaCola.formula)
print(coke_for_me.formula)
print(coke_for_you.formula)

for element in coke_for_me.formula:
    print(element)
    

coke_for_China = CocaCola()
coke_for_China.local_logo = '可口可乐'  # 创建实例属性

print(coke_for_China.local_logo)  #打印实例属性引用结果



class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']

    def drink(self):
        print('Energy!')


coke = CocaCola()
coke.drink()  # Energy!

class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']

    def drink(self, how_much):

        if how_much == 'a sip':
            print('Cool~~~')
        elif how_much == 'whole bottle':
            print('Headache!')

ice_coke = CocaCola()
ice_coke.drink('a sip')


class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']
    def __init__(self):
        self.local_logo = '可口可乐'

    def drink(self):  #HERE!
        print('Energy!')

coke = CocaCola()
print(coke.local_logo)


class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']
    def __init__(self):
        for element in self.formula:
            print('Coke has {}!'.format(element))

    def drink(self):
        print('Energy!')

coke = CocaCola
coke()



class CocaCola:
    formula = ['caffeine', 'sugar', 'water', 'soda']
    def __init__(self, logo_name):
        self.local_logo = logo_name

    def drink(self):
        print('Energy!')

coke = CocaCola('可口可乐')
print(coke.local_logo)  # 可口可乐



# 类的继承

class CocaCola:
    calories = 140
    sodium = 45
    total_carb = 39
    caffeine = 34
    ingredients = [
        'High Fructose Corn Syrup',
        'Carbonated Water',
        'Phosphoric Acid',
        'Natural Flavors',
        'Caramel Color',
        'Caffeine'
    ]

    def __init__(self, logo_name):
        self.local_logo = logo_name

    def drink(self):
        print('You got {} cal energy!'.format(self.calories))

# 类的继承(Inheritance)，拿无卡可乐(caffeine-free)作为例子

class CaffeineFree(CocaCola):  # 括号中放入CocaCola表示这个类是继承于CocaCola这个父类的。CaffeineFree成为了CocaCola的子类。
    caffeine = 0
    ingredients = [
        'High Fructose Corn Syrup',
        'Carbonated Water',
        'Phosphoric Acid',
        'Natural Flavors',
        'Caramel Color',
    ]

coke_a = CaffeineFree('Cocacola-FREE')
coke_a.drink()

# Q1
class TestA:
    attr = 1
obj_a = TestA()

TestA.attr = 42
print(obj_a.attr)  # 42

# Q2
class TestA:
    attr = 1
obj_a = TestA()
obj_b = TestA()

obj_a.attr = 42
print(obj_b.attr)  # 1


# Q3
class TestA:
    attr = 1
    def __init__(self):
        self.attr = 42

obj_a = TestA()
obj_b = TestA()

print(obj_b.attr)  # 42

print(TestA.__dict__)
print(obj_a.__dict__)  # {'__module__': '__main__', 'attr': 1, '__init__': <function TestA.__init__ at 0x000002D53D05D310>, '__dict__': <attribute '__dict__' of 'TestA' objects>, '__weakref__': <attribute '__weakref__' of 'TestA' objects>, '__doc__': None}
# {'attr': 42}



obj1 = 1
obj2 = 'String!'
obj3 = []
obj4 = {}

print(type(obj1), type(obj2), type(obj3), type(4))  # <class 'int'> <class 'str'> <class 'list'> <class 'int'>

from bs4 import BeautifulSoup
soup = BeautifulSoup
print(type(soup))  # <class 'type'>



ln_path = 'C://Users/janty/Documents/学习文档/first_name.txt'
fn_path = 'C://Users/janty/Documents/学习文档/last_name.txt'
fn = []
ln1 = []  # 单字名
ln2 = []  # 双字名
with open(fn_path, 'r', encoding="UTF-8") as f:
    for line in f.readlines():
        fn.append(line.split('\n')[0])
print(fn)
with open(ln_path, 'r', encoding="UTF-8") as f:
    for line in f.readlines():
        if len(line.split('\n')[0]) == 1:
            ln1.append(line.split('\n')[0])
        else:
            ln2.append(line.split('\n')[0])

# print(ln1)
# print('='*70)  # 分割线
# print(ln2)  # 打印完成后将fn=[], ln1 = [], ln2 = []修改成元组，元组比列表更省内存
# 将打印出来的结果复制粘贴到元组中
fn = tuple(fn)
ln1 = tuple(ln1)
ln2 = tuple(ln2)

print(fn)
print(ln1)
print('=' * 70)  # 分割线
print(ln2)  # 打印完成后将fn=[], ln1 = [], ln2 = []修改成元组，元组比列表更省内存

# 定义父类

import random


class FakeUser:
    def fake_name(self, one_word=False, two_words=False):
        if one_word:
            full_name = random.choice(fn) + random.choice(ln1)
        elif two_words:
            full_name = random.choice(fn) + random.choice(ln2)
        else:
            full_name = random.choice(fn) + random.choice(ln1 + ln2)

        print(full_name)

    def fake_gender(self):
        gender = random.choice(['男', '女', '未知'])
        print(gender)


class SnsUser(FakeUser):
    def get_followers(self, few=True, a_lot=False):
        if few:
            followers = random.randrange(1, 50)

        elif a_lot:
            followers = random.randrange(200, 500)

        print(followers)


user_a = FakeUser()
user_b = SnsUser()
user_a.fake_name()
user_b.get_followers(few=True)


class FakeUser():
    def fake_name(self, amount=1, one_word=False, two_words=False):
        n = 0
        while n <= amount:
            if one_word:
                full_name = random.choice(fn) + random.choice(ln1)
            elif two_words:
                full_name = random.choice(fn) + random.choice(ln2)
            else:
                full_name = random.choice(fn) + random.choice(ln1 + ln2)
            yield full_name
            n += 1

    def fake_gender(self, amount=1):
        n = 0
        while n <= amount:
            gender = random.choice(['男', '女', '未知'])
            yield gender
            n += 1


class SnsUser(FakeUser):
    def get_followers(self, amount=1, few=True, a_lot=False):
        n = 0
        while n <= amount:
            if few:
                followers = random.randrange(1, 50)

            elif a_lot:
                followers = random.randrange(200, 500)
            yield followers
            n += 1


user_a = FakeUser()
user_b = SnsUser()
for name in user_a.fake_name(30):
    print(name)
for gender in user_a.fake_gender(30):
    print(gender)

"""