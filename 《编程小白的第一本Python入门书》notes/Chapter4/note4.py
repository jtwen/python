import img as img
import request


# 定义一个函数


def fahrenheit_converter(C):
    fahrenheit = C * 9 / 5 + 32
    return str(fahrenheit) + '˚F'


C2F = fahrenheit_converter(35)
print(C2F)  # 95.0˚F，实际是调用函数后产生的数值


# 对函数作出修改
def fahrenheit_converter(C):
    fahrenheit = C * 9 / 5 + 32
    print(str(fahrenheit) + '˚F')


print(fahrenheit_converter(35))  # None，print只是展示给我们打印结果，因为没有关键字return，此时变量C2F中所被返回的数值——什么也没有


# 在python中可以不用写return也可顺利定义一个函数并使用，只不过返回值是None。这是函数的基本设定。


def trapezoid_area(base_up, base_down, height):
    return 1 / 2 * (base_up + base_down) * height


print(trapezoid_area(1, 2, 3))  # 4.5（位置参数）
print(trapezoid_area(base_up=1, base_down=2, height=3))  # 4.5（关键词参数）
# 给一组变量赋值然后调用
base_up = 1
base_down = 2
height = 3
print(trapezoid_area(height, base_down, base_up))  # 2.5


# 区分参数的命名和变量的命名
def flashlight(battery1, battery2):
    return 'Light!'


nanfu1 = 600
nanfu2 = 600
print(flashlight(nanfu1, nanfu2))  # nanfu1，nanfu2是变量，同时也满足能够传入函数flashlight的参数。传入后就代替了原有的battery1和
# battery2且传入方式仍旧是位置参数传入。battery1, battery2只是形式上的占位符，表示函数所需的参数应该是和battery有关的变量或对象。

# 默认参数
# request.get(url, headers=header)  # 请求网站时header可填可不填
# img.save(img_new, img_format, quality=100)  # 给图片加水印时默认的水印质量是100

file = open('C://Users/janty/Desktop/test.txt', 'w')
file.write("it's good！")  # 在test.txt中写入it's good！


# 敏感词过滤器第一部分：传入参数name和msg可控制在桌面写入的文件名称和内容的函数text_create，如果桌面没有就创建一个之后再写入
def text_create(name, msg):  # 定义函数名称和参数
    desktop_path = 'C://Users/janty/Desktop/'  # 首先是桌面路径
    full_path = desktop_path + name + '.txt'
    file = open(full_path, 'w')  # 写入模式：若没有就在该路径创建一个有该名称文本，有则覆盖文本内容
    file.write(msg)  # 写入传入的参数msg
    file.close()  # 关闭文本
    print('Done')


# text_create('test', 'this is a test message.')
text_create('test0527', 'this is a test message.')


# 定义一个函数text_filter，传入参数word, censored_word和changed_word实现过滤，敏感词censored_word默认为'lame'，替换词'changed_word'
# 默认为'Awesome'
def text_filter(word, censored_word='lame', changed_word='Awesome'):
    return word.replace(censored_word, changed_word)


text_filter('Python is lame!')


# 把两个函数合并
def censored_text_create(name, msg):
    clean_msg = text_filter(msg)
    text_create(name, clean_msg)


censored_text_create('Try', 'lame!lame!lame!')  # 调用函数

